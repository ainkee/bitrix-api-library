<?php

namespace BitrixLib\Client;

interface ClientInterface {
    public function call(string $method, array $params): array;
}
