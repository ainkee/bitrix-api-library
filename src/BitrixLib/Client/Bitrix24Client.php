<?php

namespace BitrixLib\Client;

use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс для взаимодействия с API Bitrix24.
 */
class Bitrix24Client implements ClientInterface
{
    /**
     * HTTP клиент для отправки запросов.
     * @var Client
     */
    private Client $httpClient;

    /**
     * Базовый URL для API запросов.
     * @var string
     */
    public static string $apiUrl;

    /**
     * Конструктор Bitrix24Client.
     */
    public function __construct($apiUrl = null)
    {
        if (!isset(static::$apiUrl)) {
            static::$apiUrl = $apiUrl ?? $_ENV['BITRIX_API_URL'];
        }

        $this->httpClient = new Client([
            'allow_redirects' => [
                'max' => 10,
                'strict' => true,
                'referer' => true,
                'protocols' => ['http', 'https'],
                'track_redirects' => true
            ]
        ]);
    }

    /**
     * Отправляет запрос к API Bitrix24.
     *
     * @param string $method Метод API для вызова.
     * @param array $params Параметры для передачи в запросе.
     * @return array Ответ API в виде массива.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае сетевых ошибок.
     */
    public function call(string $method, array $params = []): array
    {
        $response = $this->httpClient->request('POST', static::$apiUrl . $method, [
            'json' => $params
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if (isset($data['error'])) {
            throw new ApiException($data['error'], $data['error_description'] ?? '');
        }

        return $data;
    }
}