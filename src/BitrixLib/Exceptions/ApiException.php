<?php

namespace BitrixLib\Exceptions;

use Exception;

/**
 * Класс исключения для обработки ошибок API.
 */
class ApiException extends Exception
{
    /**
     * Дополнительная информация об ошибке.
     * @var array|null
     */
    protected ?array $data;

    /**
     * Конструктор для ApiException.
     *
     * @param int $code Код ошибки.
     * @param string|null $message Сообщение об ошибке.
     * @param array|null $data Дополнительные данные об ошибке.
     */
    public function __construct($code, $message = null, $data = null)
    {
        parent::__construct($message, $code);
        $this->data = $data;
    }

    /**
     * Получить дополнительные данные об ошибке.
     *
     * @return array|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }
}
