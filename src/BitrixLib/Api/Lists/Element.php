<?php

namespace BitrixLib\Api\Lists;

use BitrixLib\Api\AbstractEntity;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс для работы с элементами списков Bitrix24 через REST API.
 */
class Element extends AbstractEntity
{
    /**
     * @var string Идентификатор сущности. По умолчанию устанавливает тип сущности как 'lists.element' для работы с элементами списков.
     */
    protected static string $entity = 'lists.element';

    /**
     * Получает список элементов списка по фильтру либо ID элемента.
     * @param string|int $iblockId Идентификатор списка.
     * @param array $filter Фильтр выборки элементов.
     * @param array $select Список возвращаемых полей.
     * @param string|int $elementId Идентификатор элемента.
     * @param string $iblockTypeId Тип инфоблока (по умолчанию 'lists').
     * lists - тип инфоблока списка;
     * bitrix_processes - тип инфоблока процессов;
     * lists_socnet - тип инфоблока списков групп;
     * @param array $additionalFields Дополнительные поля для заполнения.
     * @return array
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function get(string|int $iblockId,
                               array      $filter = [],
                               array      $select = ['*'],
                               string|int $elementId = '',
                               string     $iblockTypeId = 'lists',
                               array      $additionalFields = []): array
    {
        $params = [
            'IBLOCK_TYPE_ID' => $iblockTypeId,
            'IBLOCK_ID' => $iblockId,
            'FILTER' => $filter,
            'SELECT' => $select
        ];

        if ($elementId) {
            $params['ELEMENT_ID'] = $elementId;
        }

        if (!empty($additionalFields)) {
            $params = array_merge($params, $additionalFields);
        }

        return self::call(static::$entity . '.get', $params)['result'] ?? [];
    }

    /**
     * Обновляет элемент списка.
     * @param string|int $iblockId Идентификатор списка.
     * @param string|int $elementCode Код элемента.
     * @param array $fields Поля элемента.
     * @param string $iblockTypeId Тип инфоблока (по умолчанию 'lists').
     * lists - тип инфоблока списка;
     * bitrix_processes - тип инфоблока процессов;
     * lists_socnet - тип инфоблока списков групп;
     * @param array $additionalFields Дополнительные поля для заполнения.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function update(string|int $iblockId,
                                  string|int $elementCode,
                                  array      $fields,
                                  string     $iblockTypeId = 'lists',
                                  array      $additionalFields = []): array|string
    {
        $params = [
            'IBLOCK_TYPE_ID' => $iblockTypeId,
            'IBLOCK_ID' => $iblockId,
            'ELEMENT_CODE' => $elementCode,
            'FIELDS' => $fields
        ];

        if (!empty($additionalFields)) {
            $params = array_merge($params, $additionalFields);
        }

        return self::call(static::$entity . '.update', $params)['result'] ?? '';
    }

    /**
     * Добавляет элемент списка.
     * @param string|int $iblockId Идентификатор списка.
     * @param array $fields Поля элемента.
     * @param string $iblockTypeId Тип инфоблока (по умолчанию 'lists').
     * lists - тип инфоблока списка;
     * bitrix_processes - тип инфоблока процессов;
     * lists_socnet - тип инфоблока списков групп;
     * @param string|int $elementCode Код элемента.
     * @param array $additionalFields Дополнительные поля для заполнения.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function add(string|int $iblockId,
                               array      $fields,
                               string     $iblockTypeId = 'lists',
                               string|int $elementCode = '',
                               array      $additionalFields = []): array|string
    {
        if (empty($elementCode)) {
            $elementCode = 'element' . time();
        }

        $params = [
            'IBLOCK_TYPE_ID' => $iblockTypeId,
            'IBLOCK_ID' => $iblockId,
            'ELEMENT_CODE' => $elementCode,
            'FIELDS' => $fields,
        ];

        if (!empty($additionalFields)) {
            $params = array_merge($params, $additionalFields);
        }

        return self::call(static::$entity . '.add', $params)['result'] ?? '';
    }

    /**
     * Удаляет элемент списка.
     * @param string|int $iblockId Идентификатор списка.
     * @param string|int $elementCode Код элемента.
     * @param string $iblockTypeId Тип инфоблока (по умолчанию 'lists').
     * lists - тип инфоблока списка;
     * bitrix_processes - тип инфоблока процессов;
     * lists_socnet - тип инфоблока списков групп;
     * @param array $additionalFields
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function delete(string|int $iblockId,
                                  string|int $elementCode,
                                  string     $iblockTypeId = 'lists',
                                  array      $additionalFields = []): array|string
    {
        $params = [
            'IBLOCK_TYPE_ID' => $iblockTypeId,
            'IBLOCK_ID' => $iblockId,
            'ELEMENT_CODE' => $elementCode,
        ];

        if (!empty($additionalFields)) {
            $params = array_merge($params, $additionalFields);
        }

        return self::call(static::$entity . '.delete', $params)['result'] ?? '';
    }
}