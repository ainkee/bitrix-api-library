<?php

namespace BitrixLib\Api;

use BitrixLib\Client\Bitrix24Client;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Абстрактный базовый класс для работы с сущностями через API Bitrix24.
 */
abstract class AbstractEntity {
    /**
     * Статический клиент для обращения к API Bitrix24.
     * @var Bitrix24Client
     */
    protected static Bitrix24Client $client;
    protected static string $forbidden = 'Вызов этого метода запрещен в классе '.self::class.'.';

    /**
     * Инициализирует клиент Bitrix24 при первом обращении к классу.
     */
    public static function init(): void {
        if (!isset(static::$client)) {
            static::$client = new Bitrix24Client();
        }
    }

    /**
     * Выполняет запрос к API Bitrix24.
     *
     * @param string $method Название метода API для вызова.
     * @param array $params Параметры для передачи в запросе.
     * @return array Ответ от API в виде массива.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок сети.
     */
    protected static function call(string $method, array $params = []): array {
        static::init();
        return static::$client->call($method, $params);
    }

    /**
     * @throws GuzzleException
     * @throws ApiException
     */
    public static function getAllEntities(string $method, array $params = [])
    {
        $start = 0;
        $params = array_merge($params, ['start' => $start]);

        $response = self::call(static::$entity . '.list', $params);
        $total = $response['total'] ?? 0;
        $result = $response['result'] ?? [];

        if ($total > 50) {
            for ($start = 50; $start < $total; $start += 50) {
                $params['start'] = $start;
                $response = self::call($method, $params);
                $result = array_merge($result, $response['result']);
            }
        }

        return $result;
    }
}