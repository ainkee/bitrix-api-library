<?php

namespace BitrixLib\Api\Catalog;

use BitrixLib\Api\BasicEntity;
use BitrixLib\Exceptions\ApiException;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс для работы с остатками в Bitrix24 через REST API.
 */
class StoreProduct extends BasicEntity
{

    /**
     * @var string Идентификатор сущности. По умолчанию устанавливает тип сущности как 'catalog.storeproduct' для работы с остатками.
     */
    protected static string $entity = 'catalog.storeproduct';

    /**
     * Возвращает описание полей сущности.
     *
     * @return array Массив с данными элемента.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function getFields(): array
    {
        return self::call(static::$entity . '.getFields');
    }

    /**
     * @throws Exception
     */
    public static function delete(int $id): array
    {
        throw new Exception(static::$forbidden);
    }

    /**
     * @throws Exception
     */
    public static function add(int|array $fields): array
    {
        throw new Exception(static::$forbidden);
    }

    /**
     * @throws Exception
     */
    public static function update(int $id, array $fields): array
    {
        throw new Exception(static::$forbidden);
    }
}