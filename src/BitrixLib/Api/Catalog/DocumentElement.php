<?php

namespace BitrixLib\Api\Catalog;

use BitrixLib\Api\BasicEntity;
use BitrixLib\Exceptions\ApiException;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс для работы с товарами документов складского учета.
 */
class DocumentElement extends BasicEntity
{

    /**
     * @var string Идентификатор сущности. По умолчанию устанавливает тип сущности как 'catalog.document.element' для работы с товарами документов.
     */
    protected static string $entity = 'catalog.document';


    /**
     *  Добавление товара к документу складского учета.
     *
     * @param array $fields Массив с полями товара.
     * Список полей:
     * 'docId' - ID документа,
     * 'storeFrom' - ID склада откуда перемещается товар,
     * 'storeTo' - ID склада куда перемещается товар,
     * 'elementId' - ID товара,
     * 'amount' - количество товара,
     * 'purchasingPrice' - закупочная цена товара.
     * @return array Массив с полями добавленного товара.
     * @throws GuzzleException
     * @throws ApiException
     */
    public static function add(array $fields): array
    {
        $params = [
            'fields' => $fields,
        ];

        return self::call(static::$entity . '.add', $params)['result']['documentElement'] ?? [];
    }

    /**
     * @throws Exception
     */
    public static function get(int $id): array
    {
        throw new Exception(static::$forbidden);
    }
}