<?php

namespace BitrixLib\Api\Catalog;

use BitrixLib\Api\BasicEntity;
use BitrixLib\Exceptions\ApiException;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс для работы с документами складского учета.
 */
class Document extends BasicEntity
{

    /**
     * @var string Идентификатор сущности. По умолчанию устанавливает тип сущности как 'catalog.document' для работы с документами.
     */
    protected static string $entity = 'catalog.document';

    /**
     *  Создание документа складского учета.
     *
     * @param array $fields Массив с полями документа.
     * Обязательные поля: 'currency' - валюта, docType - тип документа, responsibleId - ID Ответственного
     * A - Приходная накладная
     * D - Списание
     * M - Перемещение
     * R - Возврат
     * @return array Массив с полями созданного документа.
     * @throws GuzzleException
     * @throws ApiException
     */
    public static function add(array $fields): array
    {
        $params = [
            'fields' => $fields,
        ];

        return self::call(static::$entity . '.add', $params)['result']['document'] ?? [];
    }

    /**
     *  Отмена документа складского учета.
     *
     * @param string|int $id ID Документа.
     * @return array|string|null Результат отмены документа.
     * @throws GuzzleException
     * @throws ApiException
     */
    public static function cancel(string|int $id): array|string|null
    {
        $params = [
            'id' => $id,
        ];

        return self::call(static::$entity . '.cancel', $params)?? '';
    }

    /**
     *  Отмена документов складского учета.
     *
     * @param array $documentIds Массив идентификаторов документов.
     * @return array|string|null Результаты отмены документов.
     * @throws GuzzleException
     * @throws ApiException
     */
    public static function cancelList(array $documentIds): array|string|null
    {
        $params = [
            'documentIds' => $documentIds,
        ];

        return self::call(static::$entity . '.cancelList', $params)?? '';
    }

    /**
     *  Проведение документа складского учета.
     *
     * @param string|int $id ID Документа.
     * @return array|string|null Результат проведения документа.
     * @throws GuzzleException
     * @throws ApiException
     */
    public static function conduct(string|int $id): array|string|null
    {
        $params = [
            'id' => $id,
        ];

        return self::call(static::$entity . '.conduct', $params)?? '';
    }

    /**
     *  Проведение документов складского учета.
     *
     * @param array $documentIds Массив идентификаторов документов.
     * @return array|string|null Результаты проведения документов.
     * @throws GuzzleException
     * @throws ApiException
     */
    public static function conductList(array $documentIds): array|string|null
    {
        $params = [
            'documentIds' => $documentIds,
        ];

        return self::call(static::$entity . '.conductList', $params)?? '';
    }


    /**
     *  Удаление документа складского учета.
     *
     * @param string|int $id ID Документа.
     * @return array Результат удаления документа.
     * @throws GuzzleException
     * @throws ApiException
     */
    public static function delete(string|int $id): array
    {
        $params = [
            'id' => $id,
        ];

        return self::call(static::$entity . '.delete', $params) ?? [];
    }

    /**
     *  Удаление документов складского учета.
     *
     * @param array $documentIds Массив идентификаторов документов.
     * @return array|string|null Результаты удаления документов.
     * @throws GuzzleException
     * @throws ApiException
     */
    public static function deleteList(array $documentIds): array|string|null
    {
        $params = [
            'documentIds' => $documentIds,
        ];

        return self::call(static::$entity . '.deleteList', $params) ?? '';
    }

    /**
     * @throws Exception
     */
    public static function get(int $id): array
    {
        throw new Exception(static::$forbidden);
    }
}