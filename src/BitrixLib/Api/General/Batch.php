<?php

namespace BitrixLib\Api\General;

use BitrixLib\Api\AbstractEntity;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс для работы с пакетными запросами в Bitrix24 через REST API.
 */
class Batch extends AbstractEntity
{
    private const MAX_BATCH_COMMANDS = 50;
    private bool $isSleepNeeded = false;

    private array $batch = [
        "cmd" => [],
        "halt" => 0
    ];
    private array $batchResults = [];

    /**
     * Выполняет запросы из батча.
     *
     * @throws ApiException
     * @throws GuzzleException
     */
    private function executeBatch(): void
    {
        if ($this->isSleepNeeded) {
            sleep(1);
        }

        $this->isSleepNeeded = !$this->isSleepNeeded;

        $this->batchResults[] = self::call('batch', $this->batch)['result'];

        $this->batch['cmd'] = [];
    }

    /**
     * Добавляет запросы в батч.
     *
     * @param string $method Метод API.
     * @param array $fields Параметры запроса.
     *
     * @throws ApiException
     * @throws GuzzleException
     */
    public function addToBatch(string $method, array $fields): void
    {
        if (count($this->batch["cmd"]) >= self::MAX_BATCH_COMMANDS) {
            $this->executeBatch();
        }
        $this->batch['cmd'][] = "$method?" . http_build_query($fields);
    }

    /**
     * Получает результаты выполнения всех запросов в батче.
     *
     * @return array Результаты запросов.
     *
     * @throws ApiException
     * @throws GuzzleException
     */
    public function getResult(): array
    {
        if (!empty($this->batch['cmd'])) {
            $this->executeBatch();
        }

        return $this->batchResults;
    }

    /**
     * Возвращает результаты запросов, отформатированные в плоский массив.
     *
     * @return array Отформатированные результаты запросов.
     *
     * @throws ApiException
     * @throws GuzzleException
     */
    public function getFormattedResult(): array
    {
        $result = $this->getResult();
        $formattedResult = [];
        foreach ($result as $value) {
            if(isset($value['result'])) {
                foreach ($value['result'] as $subValue) {
                    if (is_array($subValue)) {
                        $formattedResult = array_merge($formattedResult, $subValue);
                    } else {
                        $formattedResult[] = $subValue;
                    }
                }
            }
        }
        return $formattedResult;
    }
}
