<?php

namespace BitrixLib\Api\General;

use BitrixLib\Api\AbstractEntity;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс User предназначен для работы с пользователями через REST API.
 */
class User extends AbstractEntity
{

    /**
     * Получает информацию о пользователе по идентификатору.
     *
     * @param int $id Идентификатор пользователя.
     * @return array Массив с информацией о пользователе.
     * @throws GuzzleException Если произошла ошибка сети.
     * @throws ApiException Если API вернул ошибку.
     */
    public static function getById(int $id): array
    {
        return self::call('user.get', ['ID' => $id])['result'];
    }

    /**
     * Получает список пользователей, соответствующих заданным фильтрам и полям выборки.
     *
     * @param array $filter Фильтры для выборки пользователей (например, ['ACTIVE' => 'Y']).
     * @param array $select Список полей пользователей, которые необходимо получить (например, ['ID', 'NAME', 'EMAIL']).
     * @return array Массив с информацией о пользователях.
     * @throws GuzzleException Если произошла ошибка сети.
     * @throws ApiException Если API вернул ошибку.
     */
    public static function getList(array $filter = [], array $select = []): array
    {
        $params = [
            'filter' => $filter,
            'select' => $select,
        ];

        return self::call('user.get', $params)['result'];
    }
}
