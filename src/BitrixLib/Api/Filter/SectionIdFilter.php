<?php

namespace BitrixLib\Api\Filter;

/**
 * Фильтр по идентификатору раздела.
 */
class SectionIdFilter implements FilterInterface
{
    private string|int $sectionId;

    /**
     * Конструктор фильтра по идентификатору раздела.
     *
     * @param string|int $sectionId Идентификатор раздела.
     */
    public function __construct(string|int $sectionId) {
        $this->sectionId = $sectionId;
    }

    /**
     * Возвращает массив критериев фильтрации по идентификатору раздела.
     *
     * @return array Массив критериев фильтрации.
     */
    public function toFilter(): array {
        return ['SECTION_ID' => $this->sectionId];
    }
}