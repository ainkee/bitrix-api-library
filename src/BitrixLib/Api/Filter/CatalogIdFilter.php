<?php

namespace BitrixLib\Api\Filter;

/**
 * Фильтр по идентификатору каталога.
 */
class CatalogIdFilter implements FilterInterface
{
    private string|int $catalogId;

    /**
     * Конструктор фильтра по идентификатору каталога.
     *
     * @param string|int $catalogId Идентификатор каталога.
     */
    public function __construct(string|int $catalogId)
    {
        $this->catalogId = $catalogId;
    }

    /**
     * Возвращает массив критериев фильтрации по идентификатору каталога.
     *
     * @return array Массив критериев фильтрации.
     */
    public function toFilter(): array
    {
        return ['CATALOG_ID' => $this->catalogId];
    }
}
