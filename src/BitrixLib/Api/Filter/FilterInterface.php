<?php

namespace BitrixLib\Api\Filter;

/**
 * Интерфейс для объектов фильтрации.
 */
interface FilterInterface
{
    /**
     * Возвращает массив критериев фильтрации.
     *
     * @return array Массив критериев фильтрации.
     */
    public function toFilter(): array;
}
