<?php

namespace BitrixLib\Api\Filter;

/**
 * Фильтр по наименованию.
 */
class NameFilter implements FilterInterface {
    private string $name;

    /**
     * Конструктор фильтра по наименованию.
     *
     * @param string $name Наименование для фильтрации.
     */
    public function __construct($name) {
        $this->name = $name;
    }

    /**
     * Возвращает массив критериев фильтрации по наименованию.
     *
     * @return array Массив критериев фильтрации.
     */
    public function toFilter(): array {
        return ['NAME' => $this->name];
    }
}
