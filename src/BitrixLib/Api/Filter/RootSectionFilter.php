<?php

namespace BitrixLib\Api\Filter;

/**
 * Фильтр для выборки корневых разделов.
 */
class RootSectionFilter implements FilterInterface
{
    /**
     * Возвращает массив критериев фильтрации для выборки корневых разделов.
     *
     * @return array Массив критериев фильтрации.
     */
    public function toFilter(): array {
        return ['SECTION_ID' => ''];
    }
}