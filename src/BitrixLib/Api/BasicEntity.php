<?php

namespace BitrixLib\Api;

use BitrixLib\Api\Filter\FilterInterface;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс для работы с сущностями в Bitrix24 через REST API.
 */
class BasicEntity extends AbstractEntity
{
    /**
     * @var string Идентификатор сущности.
     */
    protected static string $entity = '';

    /**
     * Получает список элементов сущности.
     *
     * @param array $filter Фильтр выборки элементов.
     * @param array $select Список возвращаемых полей.
     * @param array $order Параметры сортировки.
     * @param int $start Смещение для пагинации.
     * @return array Массив элементов сущности.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function getList(array $filter = [], array $select = [], array $order = [], int $start = 0): array
    {
        $params = [
            'filter' => $filter,
            'select' => $select,
            'order' => $order,
            'start' => $start,
        ];

        return self::call(static::$entity . '.list', $params)['result'] ?? [];
    }

    /**
     * Получает полный список элементов сущности.
     *
     * @param array $filter Фильтр выборки элементов.
     * @param array $select Список возвращаемых полей.
     * @param array $order Параметры сортировки.
     * @return array Массив элементов сущности.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function getAll(array $filter = [], array $select = [], array $order = []): array
    {
        $params = [
            'filter' => $filter,
            'select' => $select,
            'order' => $order,
        ];

        return parent::getAllEntities(static::$entity . '.list', $params);
    }

    /**
     * Получает отфильтрованный список сущностей.
     *
     * @param FilterInterface[] $filterObjects Массив объектов фильтрации, реализующих интерфейс FilterInterface.
     * @param array $select Массив полей, которые необходимо выбрать.
     * @param array $order Массив для определения порядка сортировки результатов.
     * @param int $start Начальный индекс для пагинации результатов.
     *
     * @return array Массив отфильтрованных сущностей.
     *
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     * @throws ApiException В случае ошибок API.
     */
    public static function getFilteredList(array $filterObjects = [], array $select = [], array $order = [], int $start = 0): array
    {
        $filter = [];

        foreach ($filterObjects as $filterObject) {
            if ($filterObject instanceof FilterInterface) {
                $filter = array_merge_recursive($filter, $filterObject->toFilter());
            }
        }

        return self::getList($filter, $select, $order, $start);
    }

    /**
     * Получает элемент сущности по его идентификатору.
     *
     * @param int $id Идентификатор элемента сущности.
     * @return array Массив с данными элемента.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function get(int $id): array
    {
        $params = [
            'ID' => $id
        ];

        return self::call(static::$entity . '.get', $params);
    }

    /**
     * Возвращает описание полей сущности.
     *
     * @return array Массив с данными элемента.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function getFields(): array
    {
        return self::call(static::$entity . '.fields');
    }

    /**
     * Удаляет элемент сущности.
     *
     * @param int $id Идентификатор удаляемого элемента сущности.
     * @return array Массив с результатом удаления.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function delete(int $id): array
    {
        $params = [
            'ID' => $id
        ];

        return self::call(static::$entity . '.delete', $params);
    }

    /**
     * Создает новый элемент в сущности.
     *
     * @param array $fields Массив полей нового элемента сущности.
     * @return array Массив с результатом создания элемента.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function add(array $fields): array
    {
        $params = [
            'fields' => $fields
        ];

        return self::call(static::$entity . '.add', $params);
    }

    /**
     * Обновляет данные элемента сущности.
     *
     * @param int $id Идентификатор обновляемого элемента сущности.
     * @param array $fields Массив обновляемых полей элемента сущности.
     * @return array Массив с результатом обновления.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function update(int $id, array $fields): array
    {
        $params = [
            'ID' => $id,
            'fields' => $fields
        ];

        return self::call(static::$entity . '.update', $params);
    }
}
