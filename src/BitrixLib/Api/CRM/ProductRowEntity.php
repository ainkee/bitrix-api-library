<?php

namespace BitrixLib\Api\CRM;

use BitrixLib\Api\BasicEntity;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

class ProductRowEntity extends BasicEntity
{
    /**
     * Возвращает товарные позиции по его идентификатору.
     *
     * @param int|string $id Идентификатор элемента сущности.
     * @return array Массив с данными элемента.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function getProductRows(int|string $id): array
    {
        $params = [
            'id' => $id
        ];

        return self::call(static::$entity . '.productrows.get', $params);
    }

    /**
     * Устанавливает товарные позиции по его идентификатору.
     *
     * @param int|string $id Идентификатор элемента сущности.
     * @param array $rows Товарные позиции - массив вида array(array("поле"=>"значение"[, ...])[, ...]).
     * Пример: [['PRODUCT_ID' => 1, 'QUANTITY' => 1, 'PRICE' => 100], ['PRODUCT_ID' => 2, 'PRODUCT_NAME' => 'Товар']].
     * @return array Массив с данными элемента.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function setProductRows(int|string $id, array $rows = []): array
    {
        $params = [
            'id' => $id,
            'rows' => $rows
        ];

        return self::call(static::$entity . '.productrows.set', $params);
    }
}