<?php

namespace BitrixLib\Api\CRM;

use BitrixLib\Api\BasicEntity;

/**
 * Класс для работы с лидами в модуле CRM Bitrix24 через REST API.
 */
class Lead extends ProductRowEntity
{
    /**
     * @var string Идентификатор сущности CRM. По умолчанию устанавливает тип сущности CRM как 'crm.lead' для работы с лидами.
     */
    protected static string $entity = 'crm.lead';
}