<?php

namespace BitrixLib\Api\CRM;

use BitrixLib\Api\AbstractEntity;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

class Item extends AbstractEntity
{
    protected static string $entity = 'crm.item';

    /**
     * Получает список элементов смарт-процесса.
     *
     * @param int|string $entityTypeId Идентификатор типа смарт-процесса.
     * @param array $filter Фильтр выборки элементов.
     * @param array $select Список возвращаемых полей.
     * @param array $order Параметры сортировки.
     * @param int $start Смещение для пагинации.
     * @return array Массив элементов смарт-процесса.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function getList(string|int $entityTypeId, array $filter = [], array $select = ['*', 'ufCrm_*'], array $order = [], int $start = 0): array
    {
        $params = [
            'entityTypeId' => $entityTypeId,
            'filter' => $filter,
            'select' => $select,
            'order' => $order,
            'start' => $start,
        ];

        return self::call(static::$entity . '.list', $params)['result']['items'] ?? [];
    }

    /**
     * Получает полный список элементов смарт-процесса.
     *
     * @param string|int $entityTypeId
     * @param array $filter Фильтр выборки элементов.
     * @param array $select Список возвращаемых полей.
     * @param array $order Параметры сортировки.
     * @return array Массив элементов смарт-процесса.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function getAll(string|int $entityTypeId, array $filter = [], array $select = ['*', 'ufCrm_*'], array $order = []): array
    {
        $params = [
            'entityTypeId' => $entityTypeId,
            'filter' => $filter,
            'select' => $select,
            'order' => $order
        ];

        return parent::getAllEntities(static::$entity . '.list', $params);
    }

    /**
     * Получает элемент смарт-процесса по его идентификатору.
     *
     * @param int $id Идентификатор элемента смарт-процесса.
     * @return array Массив с данными элемента.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function get(string|int $entityTypeId, int $id): array
    {
        $params = [
            'entityTypeId' => $entityTypeId,
            'id' => $id
        ];

        return self::call(static::$entity . '.get', $params)['result']['item'] ?? [];
    }

    /**
     * Удаляет элемент смарт-процесса.
     *
     * @param int $id Идентификатор удаляемого элемента смарт-процесса.
     * @return array Массив с результатом удаления.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function delete(string|int $entityTypeId, int $id): array
    {
        $params = [
            'entityTypeId' => $entityTypeId,
            'id' => $id
        ];

        return self::call(static::$entity . '.delete', $params);
    }

    /**
     * Создает новый элемент в смарт-процесса.
     *
     * @param array $fields Массив полей нового элемента смарт-процесса.
     * @return array Массив с результатом создания элемента.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function add(string|int $entityTypeId, array $fields): array
    {
        $params = [
            'entityTypeId' => $entityTypeId,
            'fields' => $fields
        ];

        return self::call(static::$entity . '.add', $params);
    }


    /**
     * Обновляет данные элемента смарт-процесса.
     *
     * @param int $id Идентификатор обновляемого элемента смарт-процесса.
     * @param array $fields Массив обновляемых полей элемента смарт-процесса.
     * @return array Массив с результатом обновления.
     * @throws ApiException В случае ошибок API.
     * @throws GuzzleException В случае ошибок HTTP-запроса.
     */
    public static function update(string|int $entityTypeId, int $id, array $fields): array
    {
        $params = [
            'entityTypeId' => $entityTypeId,
            'id' => $id,
            'fields' => $fields
        ];

        return self::call(static::$entity . '.update', $params);
    }
}