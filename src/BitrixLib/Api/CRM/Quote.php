<?php

namespace BitrixLib\Api\CRM;

class Quote extends ProductRowEntity
{
    /**
     * @var string Идентификатор сущности CRM. По умолчанию устанавливает тип сущности CRM как 'crm.quote' для работы с коммерческими предложениями.
     */
    protected static string $entity = 'crm.quote';
}