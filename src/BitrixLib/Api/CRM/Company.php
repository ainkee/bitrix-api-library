<?php

namespace BitrixLib\Api\CRM;

use BitrixLib\Api\BasicEntity;

/**
 * Класс для работы с сущностью "Компания" в CRM Bitrix24 через REST API.
 */
class Company extends BasicEntity
{
    /**
     * @var string Идентификатор сущности CRM. По умолчанию устанавливает тип сущности CRM как 'crm.company' для работы с компаниями.
     */
    protected static string $entity = 'crm.company';
}