<?php

namespace BitrixLib\Api\CRM;

use BitrixLib\Api\BasicEntity;

/**
 * Класс для работы с контактами в CRM Bitrix24 через REST API.
 */
class Contact extends BasicEntity
{
    /**
     * @var string Идентификатор сущности CRM. По умолчанию устанавливает тип сущности CRM как 'crm.contact' для работы с контактами.
     */
    protected static string $entity = 'crm.contact';
}