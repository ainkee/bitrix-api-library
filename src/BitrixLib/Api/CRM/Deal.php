<?php

namespace BitrixLib\Api\CRM;

use BitrixLib\Api\BasicEntity;

/**
 * Класс для работы с сделками в модуле CRM Bitrix24 через REST API.
 */
class Deal extends ProductRowEntity
{
    /**
     * @var string Идентификатор сущности CRM. По умолчанию устанавливает тип сущности CRM как 'crm.deal' для работы со сделками.
     */
    protected static string $entity = 'crm.deal';
}