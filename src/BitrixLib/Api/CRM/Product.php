<?php

namespace BitrixLib\Api\CRM;

use BitrixLib\Api\BasicEntity;

class Product extends BasicEntity
{
    /**
     * @var string Идентификатор сущности CRM. По умолчанию устанавливает тип сущности CRM как 'crm.product' для работы с товарами.
     */
    protected static string $entity = 'crm.product';
}