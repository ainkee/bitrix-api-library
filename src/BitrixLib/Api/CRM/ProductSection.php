<?php

namespace BitrixLib\Api\CRM;

use BitrixLib\Api\BasicEntity;

class ProductSection extends BasicEntity
{
    /**
     * @var string Идентификатор сущности CRM. По умолчанию устанавливает тип сущности CRM как 'crm.productsection' для работы с разделами товаров.
     */
    protected static string $entity = 'crm.productsection';
}