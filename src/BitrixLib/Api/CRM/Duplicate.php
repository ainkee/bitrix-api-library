<?php

namespace BitrixLib\Api\CRM;

use BitrixLib\Api\AbstractEntity;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс для работы с дубликатами сущностей в CRM.
 */
class Duplicate extends AbstractEntity
{

    /**
     * @var string Идентификатор сущности CRM. По умолчанию устанавливает тип сущности CRM как 'crm.duplicate' для работы с дубликатами.
     */
    protected static string $entity = 'crm.duplicate';

    /**
     *  Поиск дубликатов сущности по коммуникационным данным.
     *
     * @param array $values Строка с данными для поиска (телефоны или email-адреса).
     * @param string $type Тип коммуникации (PHONE или EMAIL).
     * @param string $entity_type Тип сущности для ограничения поиска (LEAD, CONTACT, COMPANY).
     * @return array Массив идентификаторов найденных дубликатов.
     * @throws GuzzleException
     * @throws ApiException
     */
    public static function find(array $values, string $type = 'PHONE', string $entity_type = ''): array
    {
        $params = [
            'values' => $values,
            'type' => $type,
        ];

        if ($entity_type) {
            $params['entity_type'] = $entity_type;
        }

        return self::call(static::$entity . '.findbycomm', $params)['result'] ?? [];
    }
}