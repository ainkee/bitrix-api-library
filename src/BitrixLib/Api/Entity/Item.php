<?php

namespace BitrixLib\Api\Entity;

use BitrixLib\Api\AbstractEntity;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

class Item extends AbstractEntity
{
    /**
     * @var string Идентификатор сущности. По умолчанию устанавливает тип сущности как 'entity.item'.
     */
    protected static string $entity = 'entity.item';

    /**
     * Получение списка элементов хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param array $filter Массив вида array("фильтруемое поле"=>"значение" [, ...]).
     * @param array $sort Массив для сортировки, имеющий вид by1=>order1[, by2=>order2 [, ..]], где by1, ...
     * @return array
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function get(string $entity,
                               array  $filter = [],
                               array  $sort = []): array
    {
        $params = [
            'ENTITY' => $entity,
            'FILTER' => $filter,
            'SORT' => $sort
        ];

        return self::call(static::$entity . '.get', $params)['result'] ?? [];
    }

    /**
     * Обновление элемента хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string|int $id Идентификатор элемента.
     * @param array $fields Список полей раздела хранилища.
     * NAME    Наименование элемента.
     * ACTIVE    Флаг активности элемента (Y|N).
     * DATE_ACTIVE_FROM    Дата начала активности элемента.
     * DATE_ACTIVE_TO    Дата окончания активности элемента.
     * SORT    Сортировочный вес элемента.
     * PREVIEW_PICTURE    Картинка анонса элемента.
     * PREVIEW_TEXT    Анонс элемента.
     * DETAIL_PICTURE    Детальная картинка элемента.
     * DETAIL_TEXT    Детальный текст элемента.
     * CODE    Символьный код элемента.
     * SECTION    Идентификатор раздела хранилища.
     * PROPERTY_VALUES    Обязательный. ассоциативный список значений свойств элемента. Свойства хранилища создаются при помощи entity.item.property.add.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function update(string     $entity,
                                  string|int $id,
                                  array      $fields): array|string
    {
        $params = array_merge([
            'ENTITY' => $entity,
            'ID' => $id,
        ], $fields);

        return self::call(static::$entity . '.update', $params)['result'] ?? '';
    }

    /**
     * Добавление элемента хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string $name Наименование раздела.
     * @param array $fields Список полей раздела хранилища.
     * ACTIVE    Флаг активности элемента (Y|N).
     * DATE_ACTIVE_FROM    Дата начала активности элемента.
     * DATE_ACTIVE_TO    Дата окончания активности элемента.
     * SORT    Сортировочный вес элемента.
     * PREVIEW_PICTURE    Картинка анонса элемента.
     * PREVIEW_TEXT    Анонс элемента.
     * DETAIL_PICTURE    Детальная картинка элемента.
     * DETAIL_TEXT    Детальный текст элемента.
     * CODE    Символьный код элемента.
     * SECTION    Идентификатор раздела хранилища.
     * PROPERTY_VALUES    Ассоциативный список значений свойств элемента. Свойства хранилища создаются при помощи entity.item.property.add.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function add(string $entity,
                               string $name,
                               array  $fields = []): array|string
    {
        $params = array_merge([
            'ENTITY' => $entity,
            'NAME' => $name,
        ], $fields);

        return self::call(static::$entity . '.add', $params)['result'] ?? '';
    }

    /**
     * Удаление элемента хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string|int $id Идентификатор элемента.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function delete(string $entity, string|int $id): array|string
    {
        $params = [
            'ENTITY' => $entity,
            'ID' => $id,
        ];

        return self::call(static::$entity . '.delete', $params)['result'] ?? '';
    }
}