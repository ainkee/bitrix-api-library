<?php

namespace BitrixLib\Api\Entity;

use BitrixLib\Api\AbstractEntity;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

class Property extends AbstractEntity
{
    /**
     * @var string Идентификатор сущности. По умолчанию устанавливает тип сущности как 'entity.item.property'.
     */
    protected static string $entity = 'entity.item.property';

    /**
     * Получение списка дополнительных свойств элементов хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string|int $property Строковой идентификатор свойства.
     * @return array
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function get(string $entity, string|int $property = ''): array
    {
        $params = [
            'ENTITY' => $entity,
            'PROPERTY' => $property
        ];

        return self::call(static::$entity . '.get', $params)['result'] ?? [];
    }

    /**
     * Обновление дополнительного свойства элементов хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string|int $property Строковой идентификатор свойства.
     * @param array $fields Список полей свойства.
     * PROPERTY_NEW    Новый строковой идентификатор свойства.
     * NAME    Наименование свойства.
     * TYPE    Тип свойства (S - строка, N - число, F - файл).
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function update(string     $entity,
                                  string|int $property,
                                  array      $fields): array|string
    {
        $params = array_merge([
            'ENTITY' => $entity,
            'PROPERTY' => $property,
        ], $fields);

        return self::call(static::$entity . '.update', $params)['result'] ?? '';
    }

    /**
     * Добавление дополнительного свойства элементов хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string|int $property Строковой идентификатор свойства.
     * @param string $name Наименование свойства.
     * @param string $type Тип свойства (S - строка, N - число, F - файл).
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function add(string     $entity,
                               string|int $property,
                               string     $name,
                               string     $type): array|string
    {
        $params = [
            'ENTITY' => $entity,
            'PROPERTY' => $property,
            'NAME' => $name,
            'TYPE' => $type,
        ];

        return self::call(static::$entity . '.add', $params)['result'] ?? '';
    }

    /**
     * Удаление дополнительного свойства элементов хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string|int $property Строковой идентификатор свойства.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function delete(string $entity, string|int $property): array|string
    {
        $params = [
            'ENTITY' => $entity,
            'PROPERTY' => $property
        ];

        return self::call(static::$entity . '.delete', $params)['result'] ?? '';
    }
}