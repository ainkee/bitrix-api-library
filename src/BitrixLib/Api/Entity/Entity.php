<?php

namespace BitrixLib\Api\Entity;

use BitrixLib\Api\AbstractEntity;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Класс для работы с хранилищами данных Bitrix24 через REST API.
 */
class Entity extends AbstractEntity
{
    /**
     * @var string Идентификатор сущности. По умолчанию устанавливает тип сущности как 'entity'.
     */
    protected static string $entity = 'entity';

    /**
     * Получает параметры хранилища или список всех хранилищ.
     * @param string $entity Строковой идентификатор требуемого хранилища.
     * @return array
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function get(string $entity = ''): array
    {
        $params = [];

        if ($entity) {
            $params = [
                'ENTITY' => $entity
            ];
        }

        return self::call(static::$entity . '.get', $params)['result'] ?? [];
    }

    /**
     * Обновляет параметры хранилища данных.
     * @param string $entity Строковой идентификатор обновляемого хранилища.
     * @param string $name Новое название хранилища.
     * @param array $access Описание нового набора прав доступа к хранилищу.
     * Должно иметь вид ассоциативного массива, ключами которого являются идентификаторы прав доступа, значением - R (чтение), W (запись) или X (управление).
     * AU - все авторизованные пользователи
     * U1 - пользователь с id=1
     * D1 - подразделение с id=1
     * G1 - группа с id=1
     * @param string $entityNew Новый строковой идентификатор хранилища.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function update(string $entity, string $name = '', array $access = [], string $entityNew = ''): array|string
    {
        $params = [
            'ENTITY' => $entity,
        ];

        if ($name) {
            $params['NAME'] = $name;
        }
        if (!empty($access)) {
            $params['ACCESS'] = $access;
        }
        if ($entityNew) {
            $params['ENTITY_NEW'] = $entityNew;
        }

        return self::call(static::$entity . '.update', $params)['result'] ?? '';
    }

    /**
     * Создает хранилище данных.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string $name Название хранилища.
     * @param array $access Описание прав доступа к хранилищу.
     * Должно иметь вид ассоциативного массива, ключами которого являются идентификаторы прав доступа, значением - R (чтение), W (запись) или X (управление).
     * AU - все авторизованные пользователи
     * U1 - пользователь с id=1
     * D1 - подразделение с id=1
     * G1 - группа с id=1
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function add(string $entity, string $name, array $access = []): array|string
    {
        $params = [
            'ENTITY' => $entity,
            'NAME' => $name,
            'ACCESS' => $access
        ];

        return self::call(static::$entity . '.add', $params)['result'] ?? '';
    }

    /**
     * Удаление хранилища.
     * @param string $entity Строковой идентификатор удаляемого хранилища.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function delete(string $entity): array|string
    {
        $params = [
            'ENTITY' => $entity
        ];

        return self::call(static::$entity . '.delete', $params)['result'] ?? '';
    }

    /**
     * Получение или изменение прав доступа к хранилищу.
     * @param string $entity Строковой идентификатор требуемого хранилища.
     * @param array $access Описание прав доступа к хранилищу.
     *  Должно иметь вид ассоциативного массива, ключами которого являются идентификаторы прав доступа, значением - R (чтение), W (запись) или X (управление).
     *  AU - все авторизованные пользователи
     *  U1 - пользователь с id=1
     *  D1 - подразделение с id=1
     *  G1 - группа с id=1
     * @return array
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function rights(string $entity, array $access = []): array
    {
        $params = [
            'ENTITY' => $entity
        ];

        if (!empty($access)) {
            $params['ACCESS'] = $access;
        }

        return self::call(static::$entity . '.rights', $params)['result'] ?? [];
    }
}