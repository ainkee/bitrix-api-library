<?php

namespace BitrixLib\Api\Entity;

use BitrixLib\Api\AbstractEntity;
use BitrixLib\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;

class Section extends AbstractEntity
{
    /**
     * @var string Идентификатор сущности. По умолчанию устанавливает тип сущности как 'entity.section'.
     */
    protected static string $entity = 'entity.section';

    /**
     * Получение списка разделов хранилища (секций инфоблока).
     * @param string $entity Строковой идентификатор требуемого хранилища.
     * @param array $filter Массив вида array("фильтруемое поле"=>"значение" [, ...]).
     * @param array $sort Массив для сортировки, имеющий вид by1=>order1[, by2=>order2 [, ..]], где by1, ...
     * @return array
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function get(string $entity,
                               array  $filter = [],
                               array  $sort = []): array
    {
        $params = [
            'ENTITY' => $entity,
            'FILTER' => $filter,
            'SORT' => $sort
        ];

        return self::call(static::$entity . '.get', $params)['result'] ?? [];
    }

    /**
     * Обновление раздела хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string|int $id Идентификатор обновляемого раздела.
     * @param array $fields Список полей раздела хранилища.
     * NAME    Наименование раздела.
     * DESCRIPTION    Описание раздела.
     * ACTIVE    Флаг активности раздела (Y|N).
     * SORT    Сортировочный параметр раздела.
     * PICTURE    Картинка раздела.
     * DETAIL_PICTURE    Детальная картинка раздела.
     * SECTION    Идентификатор родительского раздела.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function update(string     $entity,
                                  string|int $id,
                                  array      $fields): array|string
    {
        $params = array_merge([
            'ENTITY' => $entity,
            'ID' => $id,
        ], $fields);

        return self::call(static::$entity . '.update', $params)['result'] ?? '';
    }

    /**
     * Добавление раздела хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string $name Наименование раздела.
     * @param array $fields Список полей раздела хранилища.
     * DESCRIPTION    Описание раздела.
     * ACTIVE    Флаг активности раздела (Y|N).
     * SORT    Сортировочный параметр раздела.
     * PICTURE    Картинка раздела.
     * DETAIL_PICTURE    Детальная картинка раздела.
     * SECTION    Идентификатор родительского раздела.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function add(string $entity,
                               string $name,
                               array  $fields = []): array|string
    {
        $params = array_merge([
            'ENTITY' => $entity,
            'NAME' => $name,
        ], $fields);

        return self::call(static::$entity . '.add', $params)['result'] ?? '';
    }

    /**
     * Удаление раздела хранилища.
     * @param string $entity Строковой идентификатор хранилища.
     * @param string|int $id Идентификатор удаляемого раздела.
     * @return array|string
     * @throws ApiException
     * @throws GuzzleException
     */
    public static function delete(string $entity, string|int $id): array|string
    {
        $params = [
            'ENTITY' => $entity,
            'ID' => $id,
        ];

        return self::call(static::$entity . '.delete', $params)['result'] ?? '';
    }
}