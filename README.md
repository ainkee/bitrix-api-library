# Библиотека для работы с API Битрикса (bitrix-api-library)

Данная библиотека предназначена для облегчения интеграции и работы с API Битрикс24, позволяя разработчикам более
эффективно использовать возможности API, предоставляемые платформой Битрикс.

## Настройка

Перед началом работы с библиотекой необходимо задать переменную окружения BITRIX_API_URL, указывающую на URL API вашего
экземпляра Битрикс24. Эта переменная должна быть определена в файле .env вашего проекта. Помимо этого, убедитесь, что в
вашем проекте реализован механизм загрузки переменных окружения из .env файла в глобальный массив $_ENV. Для этого можно
использовать, например, библиотеку phpdotenv.

```dotenv
BITRIX_API_URL=ваш_url_битрикс24_api
```

## Установка

Для установки библиотеки, добавьте следующую информацию в файл `composer.json` вашего проекта:

```json
{
    "repositories": [
        {
            "type": "git",
            "url": "https://gitlab.com/ainkee/bitrix-api-library.git"
        }
    ],
    "require": {
        "ainkee/bitrix-api-library": "dev-main"
    }
}
```

После добавления указанной выше конфигурации, запустите команду:

```bash
composer update
```

## Структура библиотеки

Библиотека организована в несколько основных директорий, разделённых по функциональности:

- `src/BitrixLib/Api` - Корневая директория для всех классов, связанных с API.
- `src/BitrixLib/Api/CRM` - Содержит классы для работы с сущностями CRM, такими как Компании, Контакты, Сделки и
  Дубликаты.
- `src/BitrixLib/Api/Filter` - Классы для работы с фильтрами-объектами.
- `src/BitrixLib/Api/General` - Общие классы для операций, таких как пакетная обработка и пользовательские запросы.
- `src/BitrixLib/Api/Lists` - Классы для управления списками.
- `src/BitrixLib/Api/Entity` - Классы для работы с хранилищем данных.
- `src/BitrixLib/Client` - Клиент для взаимодействия с HTTP-клиентом и API Битрикс24.
- `src/BitrixLib/Exceptions` - Определения исключений для обработки ошибок API.

## Пример использования

Получение списка сделок:
```php
use BitrixLib\Api\CRM\Deal;

$deals = Deal::getList();
```

Работа с фильтрацией:
```php
use BitrixLib\Api\CRM\ProductSection;
use BitrixLib\Api\Filter\NameFilter;
use BitrixLib\Api\Filter\RootSectionFilter;

$nameFilter = new NameFilter('Одежда');
$rootFilter = new RootSectionFilter();

$sections = ProductSection::getFilteredList([$nameFilter, $rootFilter]);
```